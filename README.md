# CI - Computational Intelligence

## Project essentials
- name: EPANET - data optimization for compliance with the model
- link: [EPANET Main Page](https://www.epa.gov/water-research/epanet)
- desc: find and deploy an algorithm to adjust the water distribution model based on system data measurments

## Project milestones
- 1. Problem and domain analysis
- 2. Choosing toolset (benchmarking)
- 3. Setting a goal
- 4. Building a prototype - first working elements
- 5. Developing a prototype - providing first results
- 6. Project finalization - achieved goals, tests' results, output analysis
